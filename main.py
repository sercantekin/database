from model import Model

if __name__ == "__main__":
    # connection and table creation
    theModel = Model()
    theModel.dropTable()
    theModel.createTable()

    # adding records
    theUser = {"name": "n1", "email": "email@server.com", "age": 20}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))

    theUser = {"name": "cem", "email": "email2@server.com", "age": 25}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))

    theUser = {"name": "victor", "email": "email3@server.com", "age": 30}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))
    print()

    # select record by id
    print("Selected by id")
    print(theModel.selectByID(1))
    print()

    # deleting by id
    # the_model.delete(1)

    # update record
    print("Updated record")
    theModel.update(1, "sercan", "something@something", 32)
    print(theModel.selectByID(1))
    print()

    # adding some other records
    theUser = {"name": "lucas", "email": "email4@server.com", "age": 21}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))

    theUser = {"name": "solomon", "email": "email5@server.com", "age": 19}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))

    theUser = {"name": "azad", "email": "email6@server.com", "age": 45}
    insertedID = theModel.insert(theUser)
    print("Inserted ID: " + str(insertedID))
    print()

    # sorting the list by any column
    print("Ascending order by age:")
    print(theModel.sortBy("age", 1))
    print("Descending order by name:")
    print(theModel.sortBy("name", 2))
    print()

    # getting max and min records
    print("Max by age:")
    print(theModel.getMax("age"))
    print("Min by name:")
    print(theModel.getMin("name"))
    print()

    # print all list
    print("All list:")
    print(theModel.selectAll())
    print()

    # closing connection
    theModel.close()
    print("Connection is closed.")
