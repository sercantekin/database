import sys

import psycopg2

from config import DATABASE


class Model:
    # in initializing, create connection object and call connection function. and then create query object
    def __init__(self):
        self.connection = None
        self.connect()
        self.cur = self.connection.cursor()  # query object after connection object

    # connect function, it uses config data from config.py
    def connect(self):
        try:
            self.connection = psycopg2.connect(database=DATABASE["DATABASE"], user=DATABASE["USER"],
                                               password=DATABASE["PASSWORD"], host=DATABASE["HOST"])
        except psycopg2.DatabaseError as e:
            print('Error %s' % e)
            sys.exit(1)
        print("Connected.")

    # terminating connection
    def close(self):
        self.connection.close()

    # do not create the table if exists
    def dropTable(self):
        self.cur.execute("drop table if exists theuser")
        self.connection.commit()

    # create table should be run after drop_table
    def createTable(self):
        self.cur.execute(
            "CREATE TABLE theuser (id serial PRIMARY KEY, name varchar(50), email varchar(50), age integer)")
        self.connection.commit()

    # inserting data to specific table
    def insert(self, user):
        self.cur.execute("insert into theuser (name, email, age) values ('%s', '%s', '%d') returning id" %
                         (user['name'], user['email'], user['age']))  # to return id, add returning id in query
        return self.cur.fetchone()[0]

    # selecting all data from specific table
    def selectAll(self):
        self.cur.execute('select * from theuser')
        return self.cur.fetchall()

    # deleting data from specific table
    def delete(self, id):
        self.cur.execute("delete from theuser where id='%d'" % id)

    # modifying data in specific table
    def update(self, id, name, email, age):
        self.cur.execute("UPDATE theuser SET name='%s', email='%s', age='%d' WHERE id='%d'" % (name, email, age, id))

    # select by id
    def selectByID(self, id):
        self.cur.execute("select id, name, email, age from theuser where id='%d'" % id)
        return self.cur.fetchone()

    # find max age
    def getMax(self, column):
        self.cur.execute("select id, name, email, age from theuser where " + column + "=(select max(" + column + ") from theuser)")
        return self.cur.fetchone()

    # find max age
    def getMin(self, column):
        self.cur.execute("select id, name, email, age from theuser where " + column + "=(select min(" + column + ") from theuser)")
        return self.cur.fetchone()

    # sort by age
    def sortBy(self, column, sortingType):
        if sortingType == 1: self.cur.execute("select * from theuser order by " + column + " ASC")
        elif sortingType == 2: self.cur.execute("select * from theuser order by " + column + " DESC")
        else: print("Not valid sorting option. For ascending order: 1. For descending order: 2.")
        return self.cur.fetchall()
